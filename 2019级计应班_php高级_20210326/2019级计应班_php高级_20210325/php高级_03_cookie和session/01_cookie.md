## 本节目标

1. 了解cookie概念
2. 熟悉cookie的设置、获取和删除

## 什么是cookie

cookie 常用于识别用户。**cookie 是一种服务器留在用户计算机上的小文件**。

每当同一台计算机通过浏览器请求页面时，这台计算机将会发送 cookie。

通过 PHP，您能够创建并取回 cookie 的值。

### cookie的流程

![](01_cookie_files/1.jpg)

1. 请求服务端，服务端设置cookie到http返回头。
2. 客户端接收到cookie信息，在本地保存cookie。
3. 客户端再次请求服务端，会带上cookie信息。

## 设置cookie

php中使用 setcookie() 函数设置 cookie，cookie是一个键值对的数据。

语法：

	setcookie($name, $value, $expire, $path, $domain);
	
参数说明：

	$name 表示键。
	$value 表示值。
	$expire 表示过期时间，是一个时间戳。
	$path 表示作用路径。
	$domain 表示作用域名。

返回值：

	返回布尔值，设置成功返回 true，设置失败返回 false。

示例1，在下面的例子中，我们将创建名为 "userId" 的 cookie，并为它赋值 1，在一小时后过期：

```php
setcookie("userId", 1, time() + 3600);
```

请求页面，查看http响应头，可以看到返回了刚才创建的cookie。

![](01_cookie_files/2.jpg)

示例2，设置用户名 "userName" 的 cookie，并赋值为：上帝打手，并且永不过期：

```php
setrawcookie("userName", "上帝打手");
```

![](01_cookie_files/3.jpg)

这边%开头的字符，是对上帝打手的转义，可以不管。

大家可以看下这边设置userName和前面设置userId有什么不一样的地方？

 ## 获取cookie

cookie 设置好了之后，怎么获取cookie呢？PHP 的 $_COOKIE 变量用于取回 cookie 的值。

示例1，获取刚刚设置的名为 "userId" 的cookie，并把它显示在了页面上：

```php
// 输出 cookie 值
echo $_COOKIE["userId"];

// 查看所有 cookie
print_r($_COOKIE);
```

查看http请求头，可以看到浏览器带上了cookie信息：

![](01_cookie_files/4.jpg)

示例2，我们使用 empty() 函数来确认是否已设置了名称为 "userName" 的 cookie：

```html
<html>
<head>
<meta charset="utf-8">
<title>cookie获取测试</title>
</head>
<body>

<?php
if (!empty($_COOKIE["userName"]))
    echo "欢迎 " . $_COOKIE["userName"] . "!<br>";
else
    echo "普通访客!<br>";
?>

</body>
</html>
```

## 删除cookie

php中没有直接删除cookie得函数，但是通过设置过期时间可以变相实现。 

示例1，删除名称为 "userName" 的 cookie，使过期日期变更为过去的时间点：

```php
// 设置 cookie 过期时间为过去 1 小时
setcookie("userName", "", time()-3600);
```

运行代码查看http响应头，可以发现名称为 "userName" 的 cookie被删除了：

![](01_cookie_files/5.jpg)

大家可以看下是否还能取到userName这个cookie？

## 总结

本章主要就是讲解cookie的操作：

	1. 设置cookie，使用setcookie()函数。
	2. 获取cookie，使用$_COOKIE超全局变量。
	3. 删除cookie，设置cookie时间过期即可。

## 练习

1. 完成课堂上讲解的习题，并通过浏览器调试工具进行观察。
2. 设计一个程序，当用户登录成功后，记录下用户登录的时间，并显示在页面。