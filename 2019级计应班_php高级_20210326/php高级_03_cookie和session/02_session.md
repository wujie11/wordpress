## 本节目标

1. 了解session概念
2. 熟悉session的设置、获取和删除

## 什么是session

您在计算机上操作某个应用程序时，您打开它，做些更改，然后关闭它，这很像一次对话（session）。

计算机知道您是谁，它清楚您在何时打开和关闭应用程序。

然而，在因特网上问题出现了：由于 HTTP 地址无法保持状态，Web 服务器并不知道您是谁以及您做了什么。

PHP session 解决了这个问题，它通过在服务器上存储用户信息以便随后使用（比如用户名称、购买商品等）。

然而，会话信息是临时的，在用户离开网站后将被删除。如果您需要永久存储信息，可以把数据存储在数据库中。

session 的工作机制是：为每个访客创建一个唯一的 id (PHPSESSID)，并基于这个 PHPSESSID 来存储变量。

PHPSESSID 存储在 cookie 中，或者通过 URL 进行传导。

## 开启session

在使用session前，首先必须启动会话：

```php
session_start();
```

上面的代码会向服务器注册用户的会话，以便您可以开始保存用户信息，同时会为用户会话分配一个 PHPSESSID。

![](02_session_files/1.jpg)

## 设置session

session开启之后就可以使用了，直接使用全局变量 $_SESSION 设置就可以了。

示例1，设置名称为 userId 的值为 1：

```php
$_SESSION['userId'] = 1;
```

示例2，设置名称为 userName 的值为 上帝打手：

```php
$_SESSION['userName'] = "上帝打手";
```

## 获取session值

和设置类似，直接使用 $_SESSION 获取就可以了：

```html
<?php
session_start(); // session使用前需要先开启。
?>
<html>
<head>
    <meta charset="utf-8">
    <title>cookie获取测试</title>
</head>
<body>

<?php
if (!empty($_SESSION["userName"]))
    echo "欢迎 " . $_SESSION["userName"] . "!<br>";
else
    echo "普通访客!<br>";
?>

</body>
</html>
```

## 删除session

### 使用unset()删除单个session

示例1，使用unset()删除userId：

```php
unset($_SESSION['userId']);
```

### 使用session_destroy()删除所有session

```php
session_destroy();
```

## 查看session信息

通过phpinfo可以查看session信息：

![](02_session_files/2.jpg)

其中：

	session.name 上面cookie的名字。
	session.save_path 表示session存储的目录。

上例 session.save_path 为空，一般window默认在：C:\Windows\Temp目录。

## 总结

本章主要就是讲解session的操作：

	1. 设置session，使用 $_SESSION 超全局变量。
	2. 获取session，使用 $_SESSION 超全局变量。
	3. 删除session，使用unset()或者session_destroy()删除。

## 练习

1. 完成课堂上讲解的习题，并通过浏览器调试工具进行观察。
2. 设计一个程序，可以记录每个用户访问页面的次数，每个用户分别记录。
	1. 例如用户1第一次访问index.php页面，那么次数为1，再次访问那么为2。
	2. 用户2和用户1的访问次数分开计算。
