<?php
// 获取到文章数据
$articleTitle = $_POST['article_title'];
$categoryId = $_POST['category_id'];
$intro = $_POST['intro'];
$content = $_POST['content'];

if (mb_strlen($articleTitle) < 5 || mb_strlen($articleTitle) > 50) {
    echo '文章标题限制5~50个字';
    echo '<a href="javascript:void(0)" onclick="history.back();">返回上一页</a>';
    exit();
}
// 10~100
if (mb_strlen($intro) < 10 || mb_strlen($intro) > 100) {
    echo '简介限制10~100个字';
    echo '<a href="javascript:void(0)" onclick="history.back();">返回上一页</a>';
    exit();
}
// 10~8000
if (mb_strlen($content) < 10 || mb_strlen($content) > 8000) {
    echo '文章内容10~8000个字。';
    echo '<a href="javascript:void(0)" onclick="history.back();">返回上一页</a>';
    exit();
}
if (empty($categoryId)) {
    echo '需要选择文章分类。';
    echo '<a href="javascript:void(0)" onclick="history.back();">返回上一页</a>';
    exit();
}

// 保存到数据库
date_default_timezone_set("PRC");

$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db->exec("set names utf8mb4");

$addTime = time();
$updateTime = $addTime;

$sql = "insert into article (article_title, category_id, intro, content, add_time, update_time) 
  values ('$articleTitle', '$categoryId', '$intro', '$content', '$addTime', '$updateTime')";
$result = $db->exec($sql);
//var_dump($db->errorInfo());
if ($result) {
    echo "插入成功。<a href='article_list.php'>返回列表页</a>";
    exit();
} else {
    echo "插入失败，错误信息：" . $db->errorInfo()[2] . "，请联系管理员：303410541@qq.com";
}
