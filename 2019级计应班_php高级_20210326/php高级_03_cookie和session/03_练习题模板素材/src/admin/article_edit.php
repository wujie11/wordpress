<?php
date_default_timezone_set("PRC");

$articleId = $_GET['article_id'];

$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db->exec("set names utf8mb4");

// 查询分类列表
$sql = "SELECT * FROM category order by category_id desc";
$result = $db->query($sql);
$categoryList = $result->fetchAll(PDO::FETCH_ASSOC);

// 查询文章详情
$sql = "SELECT * FROM article where article_id='$articleId'";
$result = $db->query($sql);
$article = $result->fetch(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="./css/main.css"/>
</head>
<body>
<div id="container">
    <div id="header">
        <h1>博客管理系统</h1>
        <!-- <div id="admin-info">欢迎你：admin <a href="#">退出登录</a></div> -->
    </div>
    <div id="left">
        <ul>
            <li><a href="category_list.php">分类管理</a></li>
            <li><a href="article_list.php">文章管理</a></li>
            <!-- <li><a href="#">管理员</a></li> -->
        </ul>
    </div>
    <div id="right">
        <div id="right-content">
            <div id="breadcrumb-nav">
                <a href="article_list.php">首页</a> &gt;
                <a href="article_list.php">文章管理</a> &gt;
                <a href="article_add.php">增加文章</a>
            </div>
            <div id="table-add">
                <form action="article_edit_save.php" method="post">
                    <table border="" cellspacing="" cellpadding="">
                        <input type="hidden" name="article_id" value="<?php echo $article['article_id']; ?>" />
                        <tr>
                            <td>文章标题：</td>
                            <td><input type="text" name="article_title" value="<?php echo $article['article_title']; ?>"/></td>
                        </tr>
                        <tr>
                            <td>所属分类：</td>
                            <td>
                                <select name="category_id">
                                    <?php foreach ($categoryList as $row): ?>
                                        <option value="<?php echo $row['category_id']; ?>" <?php echo $article['category_id'] == $row['category_id'] ? 'selected="selected"': ''; ?>>
                                            <?php echo $row['category_name'] ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>文章简介：</td>
                            <td>
                                <textarea name="intro"><?php echo $article['intro']; ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>文章内容：</td>
                            <td>
                                <textarea name="content"><?php echo $article['content']; ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" value="提交" class="btn"/>
                                <input type="reset" value="重置" class="btn"/>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>


