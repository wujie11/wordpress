<?php
/**
 * 分类增加保存功能
 */

// 获取到分类数据
$categoryName = $_POST['category_name'];
$categoryDesc = $_POST['category_desc'];

// 2~45
if (mb_strlen($categoryName) < 2 || mb_strlen($categoryName) > 45) {
    echo '分类名称2~45个字符。';
    echo '<a href="javascript:void(0)" onclick="history.back();">返回上一页</a>';
    exit();
}
// 10~255
if (mb_strlen($categoryDesc) < 10 || mb_strlen($categoryDesc) > 255) {
    echo '分类描述10~255个字符。';
    echo '<a href="javascript:void(0)" onclick="history.back();">返回上一页</a>';
    exit();
}

// 保存到数据库
date_default_timezone_set("PRC");

$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db->exec("set names utf8mb4");

// 判断是否有同名称的分类？

$addTime = time();
$updateTime = $addTime;

$sql = "insert into category (category_name, category_desc, add_time, update_time) 
  values ('$categoryName', '$categoryDesc', '$addTime', '$updateTime')";
$result = $db->exec($sql);
//var_dump($db->errorInfo());
if ($result) {
    echo "插入成功。<a href='category_list.php'>返回列表页</a>";
    exit();
} else {
    echo "插入失败，错误信息：" . $db->errorInfo()[2]."，请联系管理员：303410541@qq.com";
}
