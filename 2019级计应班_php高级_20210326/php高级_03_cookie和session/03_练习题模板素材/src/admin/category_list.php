<?php
date_default_timezone_set("PRC");

$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, "root", "123456");

$sql = "SELECT * FROM category order by category_id desc";
$result = $db->query($sql);
$categoryList = $result->fetchAll(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="./css/main.css"/>
</head>
<body>
<div id="container">
    <div id="header">
        <h1>博客管理系统</h1>
        <!-- <div id="admin-info">欢迎你：admin <a href="#">退出登录</a></div> -->
    </div>
    <div id="left">
        <ul>
            <li><a href="category_list.php">分类管理</a></li>
            <li><a href="article_list.php">文章管理</a></li>
            <!-- <li><a href="#">管理员</a></li> -->
        </ul>
    </div>
    <div id="right">
        <div id="right-content">
            <div id="breadcrumb-nav">
                <a href="article_list.php">首页</a> &gt;
                <a href="category_list.php">分类管理</a> &gt;
                <a href="category_list.php">分类列表</a>
            </div>
            <div id="table-menu">
<!--                <button id="choose-all" class="btn">全选</button>-->
<!--                <a href="#">删除选中任务</a>-->
                <a href="category_add.php" id="add-article">增加分类</a>
            </div>
            <div id="table-list">
                <table border="" cellspacing="" cellpadding="">
                    <tr>
                        <th></th>
                        <th>分类Id</th>
                        <th>分类名称</th>
                        <th>增加时间</th>
                        <th>修改时间</th>
                        <th>操作</th>
                    </tr>
                    <?php foreach ($categoryList as $row): ?>
                        <tr>
                            <td><input type="checkbox"/></td>
                            <td><?php echo $row['category_id']; ?></td>
                            <td><?php echo $row['category_name']; ?></td>
                            <td><?php echo date("Y-m-d H:i:s", $row['add_time']); ?></td>
                            <td><?php echo date("Y-m-d H:i:s", $row['update_time']); ?></td>
                            <td>
                                <a href="category_edit.php?category_id=<?php echo $row['category_id']; ?>">编辑</a>
                                <a href="category_delete.php?category_id=<?php echo $row['category_id']; ?>">删除</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>
