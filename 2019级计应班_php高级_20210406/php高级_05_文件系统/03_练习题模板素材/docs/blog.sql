/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50730
Source Host           : 127.0.0.1:3306
Source Database       : blog

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2021-03-29 16:01:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `admin_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增长',
  `admin_name` varchar(50) NOT NULL DEFAULT '' COMMENT '姓名 ',
  `admin_email` varchar(100) NOT NULL DEFAULT '' COMMENT '用户邮箱，登录账号',
  `admin_password` varchar(20) NOT NULL DEFAULT '' COMMENT '用户密码',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '修改时间',
  `add_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '增加时间',
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', '上帝打手', '303410541@qq.com', '123456', '1617001766', '1617001766');

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `article_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL COMMENT '所属分类',
  `article_title` varchar(45) NOT NULL COMMENT '标题',
  `intro` varchar(200) NOT NULL COMMENT '文章简介',
  `content` longtext NOT NULL COMMENT '文章内容',
  `update_time` int(11) NOT NULL COMMENT '更新时间',
  `add_time` int(11) NOT NULL COMMENT '增加时间',
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COMMENT='文章表';

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES ('1', '1', 'mysql索引', 'mysql索引介绍', 'mysql索引语法', '1616399101', '1616399101');

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `category_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(45) NOT NULL COMMENT '分类名称',
  `category_desc` varchar(255) NOT NULL COMMENT '分类描述',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '记录更新时间',
  `add_time` int(11) NOT NULL COMMENT '记录增加时间',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='分类表';

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('1', 'mysql', 'mysql基础知识', '1616397910', '1616394376');
INSERT INTO `category` VALUES ('7', 'html&css', 'html和css基础知识', '1616396857', '1616396857');
