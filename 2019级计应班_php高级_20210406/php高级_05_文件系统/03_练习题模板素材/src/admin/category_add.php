<?php
/**
 * 分类增加功能
 */

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="css/main.css" />
    <link rel="stylesheet" type="text/css" href="css/top.css" />
    <link rel="stylesheet" type="text/css" href="css/left.css" />
    <link rel="stylesheet" type="text/css" href="css/right.css" />
</head>
<body>
<div id="container">
    <div id="header">
				<h1>博客管理系统</h1>
				<div id="login_info">
					欢迎你：admin
					<a href="logout.html">退出登录</a>
				</div>
			</div>
    <div id="left">
        <ul>
            <li><a href="category_list.php">分类管理</a></li>
            <li><a href="article_list.php">文章管理</a></li>
            <!-- <li><a href="#">管理员</a></li> -->
        </ul>
    </div>
    <div id="right">
        <div id="content">
            <div id="breadcrumb">
                <a href="article_list.php">首页</a> &gt;
                <a href="category_list.php">分类管理</a> &gt;
                <a href="category_add.php">增加分类</a>
            </div>
            <div id="table-add">
                <form action="category_add_save.php" method="post">
                    <table border="" cellspacing="" cellpadding="">
                        <tr>
                            <td>分类名称：</td>
                            <td><input type="text" name="category_name"/></td>
                        </tr>
                        <tr>
                            <td>分类描述：</td>
                            <td>
                                <textarea name="category_desc"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" value="提交" class="btn"/>
                                <input type="reset" value="重置" class="btn"/>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>

