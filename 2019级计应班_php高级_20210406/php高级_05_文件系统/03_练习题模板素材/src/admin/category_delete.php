<?php
/**
 * 分类删除功能
 */

// 获取到分类数据
$categoryId = $_GET['category_id'];

if (empty($categoryId)) {
    echo '分类id为空。';
    echo '<a href="javascript:void(0)" onclick="history.back();">返回上一页</a>';
    exit();
}

// 保存到数据库
date_default_timezone_set("PRC");

$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db->exec("set names utf8mb4");

$existSql = "select * from article where category_id='$categoryId' limit 1";
$existResult = $db->query($existSql);
$articleResult = $existResult->fetchAll();
if ($articleResult) {
    echo '分类下有文章，请先删除相关文章。';
    echo '<a href="javascript:void(0)" onclick="history.back();">返回上一页</a>';
    exit();
}

$sql = "delete from category where category_id='$categoryId'";
$result = $db->exec($sql);
//var_dump($db->errorInfo());
if ($result) {
    echo "删除成功。<a href='category_list.php'>返回列表页</a>";
    exit();
} else {
    echo "删除失败，错误信息：" . $db->errorInfo()[2]."，请联系管理员：303410541@qq.com";
}
