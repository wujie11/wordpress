<?php
$categoryId = $_GET["category_id"];

date_default_timezone_set("PRC");

$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db->exec("set names utf8mb4");

$sql = "select * from category where category_id='$categoryId'";
$result = $db->query($sql);
$category = $result->fetch(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="css/main.css" />
    <link rel="stylesheet" type="text/css" href="css/top.css" />
    <link rel="stylesheet" type="text/css" href="css/left.css" />
    <link rel="stylesheet" type="text/css" href="css/right.css" />
</head>
<body>
<div id="container">
    <div id="header">
				<h1>博客管理系统</h1>
				<div id="login_info">
					欢迎你：admin
					<a href="logout.html">退出登录</a>
				</div>
			</div>
    <div id="left">
        <ul>
            <li><a href="category_list.php">分类管理</a></li>
            <li><a href="article_list.php">文章管理</a></li>
            <!-- <li><a href="#">管理员</a></li> -->
        </ul>
    </div>
    <div id="right">
        <div id="content">
            <div id="breadcrumb">
                <a href="article_list.php">首页</a> &gt;
                <a href="category_list.php">分类管理</a> &gt;
                <a href="category_edit.php">编辑分类</a>
            </div>
            <div id="table-add">
                <form action="category_edit_save.php" method="post">
                    <table border="" cellspacing="" cellpadding="">
                        <tr>
                            <td width="190">分类id：</td>
                            <td><input type="text" value="<?php echo $category['category_id']; ?>" readonly="readonly" name="category_id"/>
                            </td>
                        </tr>
                        <tr>
                            <td>分类名称：</td>
                            <td><input type="text" name="category_name"
                                       value="<?php echo $category['category_name']; ?>"/></td>
                        </tr>
                        <tr>
                            <td>分类描述：</td>
                            <td>
                                <textarea name="category_desc"><?php echo $category['category_desc']; ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <input type="submit" value="提交" class="btn"/>
                                <input type="reset" value="重置" class="btn"/>
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>

