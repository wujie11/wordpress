<?php

$adminEmail = $_POST['admin_email'];
$adminPassword = $_POST['admin_password'];
$verifyCode = $_POST['verify_code']; //
$rememberMe = $_POST['remember_me'] ?? '';

session_start();
if ($verifyCode != $_SESSION['verify_code']) {
    echo "验证码错误。<a href='login.php'>返回登录页</a>";
    exit();
}

date_default_timezone_set("PRC"); // 设置时区

$dsn = "mysql:host=127.0.0.1;dbname=blog";
$db = new PDO($dsn, "root", "123456");
$db->exec("set names utf8mb4");

//
$sql = "SELECT * FROM admin where admin_email='{$adminEmail}'";
$result = $db->query($sql);
$admin = $result->fetch(PDO::FETCH_ASSOC);

if (!$admin) {
    echo "账户或者密码不匹配。<a href='login.php'>返回登录页</a>";
    exit();
}

if ($admin['admin_password'] == $adminPassword) {
    if (!session_id()) {
        session_start();
    }
    $_SESSION['admin_email'] = $adminEmail;
    $_SESSION['admin_name'] = $admin['admin_name'];
    if ($rememberMe) {
        setcookie('PHPSESSID', session_id(), time() + 20);
    }
    echo "登录成功。<a href='article_list.php'>前往文章列表页</a>";
    exit();
} else {
    echo "账户或者密码不匹配。<a href='login.php'>返回登录页</a>";
    exit();
}

//var_dump($articleList);

